use std::rc::{Rc, Weak};
use std::cell::RefCell;

#[derive(PartialEq, Debug, Copy, Clone)]
pub enum ProblemState {
    None,
    Started,
    Solved
}

#[derive(Debug)]
pub struct Problem {
    pub folded:         bool,
    pub state:          ProblemState,
    pub description:    String,
    pub subproblems:    Vec<Rc<RefCell<Problem>>>,
    pub parent:         Weak<RefCell<Problem>>,
}

impl Problem {
    pub fn new(parent: Weak<RefCell<Problem>>) -> Problem {
        Problem {
            folded: false,
            state: ProblemState::None,
            description: String::new(),
            subproblems: vec![],
            parent,
        }
    }

    pub fn get_depth(&self) -> usize {
        let mut depth = 0;
        let mut i = self.parent.clone();
        while let Some(parent) = i.upgrade() {
            i = parent.borrow().parent.clone();
            depth += 1;
        }
        depth
    }
}
