use crate::problem::{Problem, ProblemState};
use crate::dissolver::Dissolver;
use crate::action::Action;
use std::rc::Rc;
use std::cell::RefCell;

#[derive(Debug)]
pub struct ProblemEditDescription {
    problem: Rc<RefCell<Problem>>,
    from: String,
    to: String,
}

impl Action for ProblemEditDescription {
    fn apply_to(dissolver: &mut Dissolver) -> Result<Box<dyn Action>, ()> {
        let problem = dissolver.get_selected();
        let from = problem.borrow().description.clone();
        let to = dissolver.prompt(&from);
        problem.borrow_mut().description = to.clone();
        Ok(Box::new(ProblemEditDescription {
            problem,
            from,
            to,
        }))
    }

    fn undo(&self) {
        self.problem.borrow_mut().description = self.from.clone();
    }

    fn redo(&self) {
        self.problem.borrow_mut().description = self.to.clone();
    }
}

#[derive(Debug)]
pub struct ProblemToggleStart {
    problem: Rc<RefCell<Problem>>,
    from: ProblemState,
}

impl Action for ProblemToggleStart {
    fn apply_to(dissolver: &mut Dissolver) -> Result<Box<dyn Action>, ()> {
        let problem = dissolver.get_selected();
        let from = problem.borrow().state;
        if from == ProblemState::None {
            problem.borrow_mut().state = ProblemState::Started;
            Ok(Box::new(ProblemToggleStart {
                problem,
                from,
            }))
        } else if from == ProblemState::Started {
            problem.borrow_mut().state = ProblemState::None;
            Ok(Box::new(ProblemToggleStart {
                problem,
                from,
            }))
        } else {
            Err(())
        }
    }

    fn undo(&self) {
        self.problem.borrow_mut().state = self.from;
    }

    fn redo(&self) {
        if self.from == ProblemState::None {
            self.problem.borrow_mut().state = ProblemState::Started;
        } else {
            self.problem.borrow_mut().state = ProblemState::None;
        }
    }
}

#[derive(Debug)]
pub struct ProblemToggleSolve {
    problem: Rc<RefCell<Problem>>,
    from: ProblemState,
    was_folded: bool,
}

impl Action for ProblemToggleSolve {
    fn apply_to(dissolver: &mut Dissolver) -> Result<Box<dyn Action>, ()> {
        let problem = dissolver.get_selected();
        let from = problem.borrow().state;
        let was_folded = problem.borrow().folded;
        if from != ProblemState::Solved {
            problem.borrow_mut().state = ProblemState::Solved;
            problem.borrow_mut().folded = true;
        } else {
            problem.borrow_mut().state = ProblemState::None;
        }
        Ok(Box::new(ProblemToggleSolve {
            problem,
            from,
            was_folded,
        }))
    }

    fn undo(&self) {
        self.problem.borrow_mut().state = self.from;
        if self.from != ProblemState::Solved {
            self.problem.borrow_mut().folded = self.was_folded;
        }
    }

    fn redo(&self) {
        if self.from != ProblemState::Solved {
            self.problem.borrow_mut().state = ProblemState::Solved;
            self.problem.borrow_mut().folded = true;
        } else {
            self.problem.borrow_mut().state = ProblemState::None;
        }
    }
}

#[derive(Debug)]
pub struct ProblemToggleFold {
    problem: Rc<RefCell<Problem>>,
    was_folded: bool,
}

impl Action for ProblemToggleFold {
    fn apply_to(dissolver: &mut Dissolver) -> Result<Box<dyn Action>, ()> {
        let problem = dissolver.get_selected();
        let was_folded = problem.borrow().folded;
        problem.borrow_mut().folded = !was_folded;
        Ok(Box::new(ProblemToggleFold {
            problem,
            was_folded,
        }))
    }

    fn undo(&self) {
        self.problem.borrow_mut().folded = self.was_folded;
    }

    fn redo(&self) {
        self.problem.borrow_mut().folded = !self.was_folded;
    }
}
