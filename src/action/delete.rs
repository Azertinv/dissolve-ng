use crate::problem::Problem;
use crate::dissolver::Dissolver;
use crate::action::Action;
use std::rc::Rc;
use std::cell::RefCell;

pub struct ProblemDelete {
    parent: Rc<RefCell<Problem>>,
    problem: Rc<RefCell<Problem>>,
    index: usize,
}

impl Action for ProblemDelete {
    fn apply_to(dissolver: &mut Dissolver) -> Result<Box<dyn Action>, ()> {
        let problem = dissolver.get_selected();
        let parent = problem.borrow().parent.upgrade().unwrap();
        let index = parent.borrow().subproblems.iter().position(|x| Rc::ptr_eq(&x, &problem)).unwrap();
        parent.borrow_mut().subproblems.remove(index);
        Ok(Box::new(ProblemDelete {
            parent,
            problem,
            index,
        }))
    }

    fn undo(&self) {
        self.parent.borrow_mut().subproblems.insert(self.index, self.problem.clone());
    }

    fn redo(&self) {
        self.parent.borrow_mut().subproblems.remove(self.index);
    }
}
