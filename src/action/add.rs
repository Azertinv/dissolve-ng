use crate::problem::Problem;
use crate::dissolver::Dissolver;
use crate::action::Action;
use std::rc::Rc;
use std::cell::RefCell;

pub struct ProblemAddChildren {
    parent: Rc<RefCell<Problem>>,
    problem: Rc<RefCell<Problem>>,
}

impl Action for ProblemAddChildren {
    fn apply_to(dissolver: &mut Dissolver) -> Result<Box<dyn Action>, ()> {
        let parent = dissolver.get_selected();
        let problem = Rc::new(RefCell::new(Problem::new(Rc::downgrade(&parent))));
        parent.borrow_mut().folded = false;
        parent.borrow_mut().subproblems.push(problem.clone());
        dissolver.focus(&problem);
        problem.borrow_mut().description = dissolver.prompt("");
        Ok(Box::new(ProblemAddChildren {
            parent,
            problem,
        }))
    }

    fn undo(&self) {
        self.parent.borrow_mut().subproblems.pop();
    }

    fn redo(&self) {
        self.parent.borrow_mut().subproblems.push(self.problem.clone());
    }
}

pub struct ProblemAdd {
    parent: Rc<RefCell<Problem>>,
    problem: Rc<RefCell<Problem>>,
    index: usize,
}

impl Action for ProblemAdd {
    fn apply_to(dissolver: &mut Dissolver) -> Result<Box<dyn Action>, ()> {
        let sibling = dissolver.get_selected();
        let parent = sibling.borrow().parent.upgrade().unwrap();
        let index = parent.borrow().subproblems.iter().position(|x| Rc::ptr_eq(&x, &sibling)).unwrap() + 1;
        let problem = Rc::new(RefCell::new(Problem::new(Rc::downgrade(&parent))));
        parent.borrow_mut().subproblems.insert(index, problem.clone());
        dissolver.focus(&problem);
        problem.borrow_mut().description = dissolver.prompt("");
        Ok(Box::new(ProblemAdd {
            parent,
            problem,
            index,
        }))
    }

    fn undo(&self) {
        self.parent.borrow_mut().subproblems.remove(self.index);
    }

    fn redo(&self) {
        self.parent.borrow_mut().subproblems.insert(self.index, self.problem.clone());
    }
}
