mod dissolver;
mod problem;
mod action;

use crate::problem::Problem;
use crate::dissolver::Dissolver;

use std::rc::{Rc, Weak};
use std::cell::RefCell;

fn main() {
    let root = Rc::new(RefCell::new(Problem::new(Weak::new()))); // TODO load from file
    let mut dissolver = Dissolver::new(&root);
    dissolver.main_loop();
}
