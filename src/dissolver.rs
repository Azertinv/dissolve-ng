use crate::action::Action;
use crate::problem::Problem;

use crate::action::add::{ProblemAdd, ProblemAddChildren};
use crate::action::delete::ProblemDelete;
use crate::action::edit::{ProblemEditDescription, ProblemToggleStart, ProblemToggleSolve, ProblemToggleFold};

use std::rc::Rc;
use std::cell::RefCell;

pub struct Dissolver {
    actions:        Vec<Box<dyn Action>>,
    action_cursor:  usize,
    clipboard:      Option<Problem>,
    displayed:      Vec<Rc<RefCell<Problem>>>,
    cursor:         usize,
    root:           Rc<RefCell<Problem>>,
}

impl Dissolver {
    pub fn new(root: &Rc<RefCell<Problem>>) -> Dissolver {
        Dissolver {
            actions: vec![],
            action_cursor: 0,
            clipboard: None,
            displayed: vec![],
            cursor: 0,
            root: root.clone(),
        }
    }

    pub fn main_loop(&mut self) {
        self.draw();
        self.do_action::<ProblemAddChildren>();
        self.do_action::<ProblemAddChildren>();
        self.do_action::<ProblemAddChildren>();
        self.displayed[0].borrow_mut().description = "ayy".to_string();
        self.displayed[1].borrow_mut().description = "lmao".to_string();
        self.displayed[2].borrow_mut().description = "wassup".to_string();
        self.draw();
        self.go_down();
        self.do_action::<ProblemDelete>();
        self.go_up();
        self.undo();
        self.draw();
        self.go_down();
        self.go_down();
        self.do_action::<ProblemEditDescription>();
        self.draw();
        self.undo();
        self.draw();
        self.redo();
        self.draw();
        self.do_action::<ProblemAdd>();
        self.draw();
        self.go_up();
        self.draw();
        self.do_action::<ProblemToggleStart>();
        self.draw();
        self.undo();
        self.draw();
        self.redo();
        self.draw();
        self.go_up();
        self.draw();
        self.do_action::<ProblemToggleSolve>();
        self.draw();
        self.undo();
        self.draw();
        self.do_action::<ProblemToggleStart>();
        self.draw();
        self.do_action::<ProblemToggleSolve>();
        self.draw();
        self.do_action::<ProblemDelete>();
        self.draw();
        self.undo();
        self.draw();
        self.undo();
        self.draw();
        self.do_action::<ProblemToggleFold>();
        self.draw();
        self.undo();
        self.draw();
        self.redo();
        self.draw();
    }

    pub fn do_action<T: Action>(&mut self) {
        if let Ok(action) = T::apply_to(self) {
            if self.action_cursor < self.actions.len() {
                self.actions.truncate(self.action_cursor);
            }
            self.action_cursor += 1;
            self.actions.push(action);
        }
    }

    pub fn undo(&mut self) {
        if self.action_cursor == 0 { return }
        self.action_cursor -= 1;
        self.actions[self.action_cursor].undo();
    }

    pub fn redo(&mut self) {
        if self.action_cursor == self.actions.len() { return }
        self.actions[self.action_cursor].redo();
        self.action_cursor += 1;
    }

    pub fn draw(&mut self) {
        self.calculate_displayed();
        let mut i = 1;
        println!("================================");
        for problem in &self.displayed {
            println!("{}{}{} {}: {:?}",
                if self.cursor == i {">"} else {""},
                "\t".to_string().repeat(problem.borrow().get_depth()),
                if problem.borrow().folded {"+"} else {"-"},
                problem.borrow().description,
                problem.borrow().state);
            i += 1;
        }
    }

    fn calculate_displayed_helper(&mut self, problem: &Rc<RefCell<Problem>>) {
        if problem.borrow().folded { return }
        for subproblem in &problem.borrow().subproblems {
            self.displayed.push(subproblem.clone());
            self.calculate_displayed_helper(&subproblem);
        }
    }

    pub fn calculate_displayed(&mut self) {
        self.displayed.clear();
        self.calculate_displayed_helper(&self.root.clone());
        if self.cursor == 0 && !self.displayed.is_empty() {
            self.cursor = 1;
        }
        else if self.cursor > self.displayed.len() {
            self.cursor = self.displayed.len();
        }
    }

    pub fn get_selected(&self) -> Rc<RefCell<Problem>> {
        if self.cursor > 0 {
            self.displayed[self.cursor - 1].clone()
        } else {
            self.root.clone()
        }
    }

    pub fn go_up(&mut self) {
        self.calculate_displayed();
        if self.cursor == 0 { return }
        self.cursor -= 1;
        if self.cursor < 1 {
            self.cursor = self.displayed.len();
        }
    }

    pub fn go_down(&mut self) {
        self.calculate_displayed();
        if self.cursor == 0 { return }
        self.cursor += 1;
        if self.cursor > self.displayed.len() {
            self.cursor = 1;
        }
    }

    pub fn focus(&mut self, problem: &Rc<RefCell<Problem>>) {
        self.calculate_displayed();
        self.cursor = self.displayed.iter().position(|x| Rc::ptr_eq(&x, problem)).unwrap() + 1;
        self.draw();
    }

    pub fn prompt(&self, placeholder: &str) -> String {
        // TODO
        "ayy".to_string()
    }
}
