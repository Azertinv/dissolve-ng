pub mod edit;
pub mod delete;
pub mod add;

use crate::dissolver::Dissolver;

pub trait Action {
    fn apply_to(dis: &mut Dissolver) -> Result<Box<dyn Action>, ()> where Self: Sized;
    fn undo(&self);
    fn redo(&self);
}
